package Ex3;

import java.io.*;

public class Encryption {

	
	public void encrypt(File f1,File f2) {
		try {
			BufferedReader br = new BufferedReader(new FileReader(f1));
			PrintWriter pw = new PrintWriter( new FileWriter(f2));
			int value;
			while((value=br.read())!= -1) {
				value++;
			pw.write((char)value);
				System.out.println((char) value);
			}
			pw.close();
		
		}
		catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	public void decrypt(File f1,File f2) {
		try {
			BufferedReader br = new BufferedReader(new FileReader(f1));
			PrintWriter pw = new PrintWriter( new FileWriter(f2));
			int value;
			while((value=br.read())!= -1) {
				value--;
			pw.write((char)value);
				System.out.println((char) value);
			}
			pw.close();
		
		}
		catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	
	public static void main(String[] args ) {
		
		Encryption e = new Encryption();
		File f = new File("decrypted.txt");
		File f2 = new File("encrypted.txt");
		
		
		e.encrypt(f,f2);
		e.decrypt(f2, f);
		
		
	}
}
