package Ex1;

public class CoffeeMaker {

	public static int nbCoffeesMade=0;
	
	public Coffee makeCofee() throws TooManyCoffees{
		System.out.println("Make a coffee");
        int t = (int)(Math.random()*100);
        int c = (int)(Math.random()*100);
        if(nbCoffeesMade<3) {
        	Coffee cofee = new Coffee(t,c);
        	nbCoffeesMade++;
            return cofee;
        }
        else throw new TooManyCoffees("You made too many coffees");
    }
	
	 
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		CoffeeMaker cofMaker = new CoffeeMaker();
		try {
			cofMaker.makeCofee();
			cofMaker.makeCofee();
			cofMaker.makeCofee();
			cofMaker.makeCofee();
		}
		catch(TooManyCoffees e) {
			e.printStackTrace();
		}
		
	}

}
