package Ex4;

import java.io.*;

public class Car implements Serializable{
	public String model;
	public int price;
	
	Car(String s,int i){
		model=s;
		price=i;
	}
	
	public Car loadObject() {
		File f = new File("savedCar.txt");
		try {
			FileInputStream fos = new FileInputStream(f);
			ObjectInputStream oos = new ObjectInputStream(fos);
			return (Car) oos.readObject();
		} catch (IOException | ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	public void saveObject() {
		File f = new File("savedCar.txt");
		try {
			FileOutputStream fos = new FileOutputStream(f);
			ObjectOutputStream oos = new ObjectOutputStream(fos);
			oos.writeObject(this);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static void main(String[]args) {
		
		Car c1= new Car("Tesla",40000);
		c1.saveObject();
		Car c2= c1.loadObject();
		System.out.println(c2.model+", with a price of "+c2.price);
		
	}
	
	
	
	
	
}
