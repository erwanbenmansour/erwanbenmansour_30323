package Ex5;

import java.awt.EventQueue;
import java.awt.FileDialog;

import javax.swing.JFrame;
import javax.swing.JTextField;
import java.awt.BorderLayout;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.*;
import java.awt.GridBagConstraints;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JComboBox;
import javax.swing.SwingConstants;

import Ex1.Editor;

public class Interface {

	private JFrame frame;
	private JTextField txtTrName;
	private JTextField txtDest;
	private JTextField txtSeg;
	
	
	public ArrayList<JLabel> segmentLabels =  new ArrayList<JLabel>();
	public ArrayList<Segment> segments =  new ArrayList<Segment>();
	public ArrayList<Controler> controlers = new ArrayList<Controler>();


	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Interface window = new Interface();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Interface() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		
		//Initialize Segments
		for(int i=1;i<=9;i++) {
			segments.add(new Segment(i));
		}
		
		//Initialize Controlers
		controlers.add(new Controler("Cluj-Napoca"));
		controlers.add(new Controler("Paris-Gare de l'Est"));
		controlers.add(new Controler("Bucuresti"));
		
		for(Controler cont: controlers) {
			for(Controler cont2: controlers) {
				if(cont2!=cont) {
					cont.setNeighbourController(cont2);
				}
			}
		}
		for(Segment s : segments) {
			if(s.id<4) {
				controlers.get(0).addControlledSegment(s);
			}
			if(s.id>7) {
				controlers.get(2).addControlledSegment(s);
			}
			else controlers.get(1).addControlledSegment(s);
		}
		
		
		
		frame = new JFrame();
		frame.setBounds(100, 100, 700, 500);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Train Name");
		lblNewLabel.setBounds(349, 64, 108, 24);
		frame.getContentPane().add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("Destination");
		lblNewLabel_1.setBounds(349, 105, 108, 24);
		frame.getContentPane().add(lblNewLabel_1);
		
		JLabel lblSegment = new JLabel("Segment");
		lblSegment.setBounds(349, 147, 108, 24);
		frame.getContentPane().add(lblSegment);
		
		txtTrName = new JTextField();
		txtTrName.setBounds(467, 66, 166, 20);
		frame.getContentPane().add(txtTrName);
		txtTrName.setColumns(10);
		
		txtDest = new JTextField();
		txtDest.setBounds(467, 107, 166, 20);
		frame.getContentPane().add(txtDest);
		txtDest.setColumns(10);
		
		txtSeg = new JTextField();
		txtSeg.setBounds(467, 149, 166, 20);
		frame.getContentPane().add(txtSeg);
		txtSeg.setColumns(10);
		
		JButton btnStep = new JButton("Next Step");
		btnStep.setBounds(349, 350, 108, 23);
		frame.getContentPane().add(btnStep);
		btnStep.addActionListener(new NextStep());
		
		JLabel labParis = new JLabel("Paris-Gare de l'Est");
		labParis.setHorizontalAlignment(SwingConstants.CENTER);
		labParis.setBounds(10, 69, 145, 14);
		frame.getContentPane().add(labParis);
		
		JLabel s1 = new JLabel("S(1)-");
		s1.setBounds(10, 105, 257, 14);
		frame.getContentPane().add(s1);
		segmentLabels.add(s1);
		
		JLabel s2 = new JLabel("S(2)-");
		s2.setBounds(10, 129, 257, 14);
		frame.getContentPane().add(s2);
		segmentLabels.add(s2);
		
		JLabel s3 = new JLabel("S(3)-");
		s3.setBounds(10, 152, 257, 14);
		frame.getContentPane().add(s3);
		segmentLabels.add(s3);
		
		JLabel labCluj = new JLabel("Cluj-Napoca");
		labCluj.setHorizontalAlignment(SwingConstants.CENTER);
		labCluj.setBounds(33, 193, 89, 14);
		frame.getContentPane().add(labCluj);
		
		JLabel s4 = new JLabel("S(4)-");
		s4.setBounds(10, 229, 257, 14);
		frame.getContentPane().add(s4);
		segmentLabels.add(s4);
		
		JLabel s5 = new JLabel("S(5)-");
		s5.setBounds(10, 253, 257, 14);
		frame.getContentPane().add(s5);
		segmentLabels.add(s5);
		
		JLabel s6 = new JLabel("S(6)-");
		s6.setBounds(10, 276, 257, 14);
		frame.getContentPane().add(s6);
		segmentLabels.add(s6);
		
		JLabel labBuc = new JLabel("Bucuresti");
		labBuc.setHorizontalAlignment(SwingConstants.CENTER);
		labBuc.setBounds(33, 318, 89, 14);
		frame.getContentPane().add(labBuc);
		
		JLabel s7 = new JLabel("S(7)-");
		s7.setBounds(10, 354, 257, 14);
		frame.getContentPane().add(s7);
		segmentLabels.add(s7);
		
		JLabel s8 = new JLabel("S(8)-");
		s8.setBounds(10, 378, 257, 14);
		frame.getContentPane().add(s8);
		segmentLabels.add(s8);
		
		JLabel s9 = new JLabel("S(9)-");
		s9.setBounds(10, 401, 257, 14);
		frame.getContentPane().add(s9);
		segmentLabels.add(s9);
		
		JButton btnAddTrain = new JButton("Add Train");
		btnAddTrain.addActionListener(new AddTrain());
		btnAddTrain.setBounds(349, 189, 108, 23);
		frame.getContentPane().add(btnAddTrain);
		
		JLabel lblNewLabel_7 = new JLabel("Train Simulator");
		lblNewLabel_7.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel_7.setBounds(10, 11, 666, 14);
		frame.getContentPane().add(lblNewLabel_7);
	}
	
	class AddTrain implements ActionListener
    {
          public void actionPerformed(ActionEvent e)
          {
        	
             String trName= txtTrName.getText();
             String seg= txtSeg.getText();
             String dest = txtDest.getText();
             int numSeg= Integer.valueOf(seg);
             JLabel modifSeg=segmentLabels.get(numSeg-1);
             modifSeg.setText("S("+(numSeg)+")- "+trName+" to "+dest);
             
             Segment s = segments.get(numSeg-1);
             s.arriveTrain(new Train(dest,trName));
             
             
             
             

          }
    }
	class NextStep implements ActionListener
    {
          public void actionPerformed(ActionEvent e)
          {
        	
            for(Controler cont: controlers) {
            	cont.controlStep();
            }
             
            updateView();
            
             
             

          }
    }
	
	void updateView() {
		for(Segment s : segments) {
			if(s.hasTrain()) {
				
				JLabel sLab = segmentLabels.get(segments.indexOf(s));
				
				
				String trName= s.getTrain().name;
	            String dest = s.getTrain().getDestination();
	            int numSeg= s.id;
	            
	            
	            
	            System.out.println("Updated train "+trName+" on segment "+numSeg );
	            
	            sLab.setText("S("+(numSeg)+")- "+trName+" to "+dest);
				
			}
			else {
				int numSeg= s.id;
				JLabel sLab = segmentLabels.get(segments.indexOf(s));
				sLab.setText("S("+(numSeg)+")-");
				
			}
		}
	}
	
	
}


