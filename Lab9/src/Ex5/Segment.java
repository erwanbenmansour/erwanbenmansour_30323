
package Ex5;
import Ex5.Train;

class Segment{
	int id;
	Train train;
 
	Segment(int id){
		this.id = id;
	}
 
	boolean hasTrain(){
		return train!=null;
	}
 
	Train departTRain(){
		Train tmp = train;
		this.train = null;
		return tmp;
	}
 
	void arriveTrain(Train t){
		train = t;
	}
 
	Train getTrain(){
		return train;
	}
}