package Ex4;

import java.awt.GridLayout;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.*;
import java.util.*;

import javax.swing.*;

public class XO extends JFrame {
	
	ArrayList<JButton> buttons =  new ArrayList<JButton>();
	
	JTextArea mes;
	
	char playerturn='X';
	
	
	 XO(String title){
		 
         super(title);
         init();
         setVisible(true);
   }
	 
	 
	 void init() {
		 
		 setLayout(new GridLayout(4,3));
		 for(int i=0;i<9;i++) {
			 JButton but = new JButton("");
			 but.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					// TODO Auto-generated method stub
					String value=but.getText();
					if(value.equals("")) {
						if(playerturn=='X') {
							System.out.println("Setting x");
							but.setText("X");
							playerturn='Y';
							
						}
						else if(playerturn=='Y') {
							System.out.println("Setting y");
							but.setText("Y");
							playerturn='X';
						}
						checkVictory();
					};
					
				}
				 
			 });
			 
			 
			 
			 buttons.add(but);
			 add(but);
			
		 }
		 mes = new JTextArea();
		 mes.setBounds(0,300,400,100);
		
		 add(mes);
		 setSize(400,400);
		 
	 }
	 
	 
	 void checkVictory(){
		 
		 ///////////////////////////////////////
		 //Check if Y is the winner
		 String winner= "Y";
		 
		 //Check vertical
		 for(int i=0;i<3;i++) {
			 if(buttons.get(0).getText().equals(winner) && buttons.get(i+3).getText().equals(winner) && buttons.get(i+6).getText().equals(winner)) {
				mes.setText("Y is the winner"); 
			 }
			 
		 }
		 //Check Horizontal
		 for(int i=0;i<9;i+=3) {
			 if(buttons.get(i).getText().equals(winner) && buttons.get(i+1).getText().equals(winner) && buttons.get(i+2).getText().equals(winner)) {
				 mes.setText("Y is the winner"); 
			 }
			 
		 }
		//Check Diagonal
		if(buttons.get(0).getText().equals(winner) && buttons.get(4).getText().equals(winner) && buttons.get(8).getText().equals(winner)) {
			 mes.setText("Y is the winner"); 
		 }
		else if(buttons.get(2).getText().equals(winner) && buttons.get(4).getText().equals(winner) && buttons.get(6).getText().equals(winner)) {
			 mes.setText("Y is the winner"); 
		 }
			 
		/////////////////////////////
		
		//Check if X is the winner
		winner= "X";
		 
		 //Check vertical
		 for(int i=0;i<3;i++) {
			 if(buttons.get(0).getText().equals(winner) && buttons.get(i+3).getText().equals(winner) && buttons.get(i+6).getText().equals(winner)) {
				mes.setText("X is the winner"); 
			 }
			 
		 }
		 //Check Horizontal
		 for(int i=0;i<9;i+=3) {
			 if(buttons.get(i).getText().equals(winner) && buttons.get(i+1).getText().equals(winner) && buttons.get(i+2).getText().equals(winner)) {
				 mes.setText("X is the winner"); 
			 }
			 
		 }
		//Check Diagonal
		if(buttons.get(0).getText().equals(winner) && buttons.get(4).getText().equals(winner) && buttons.get(8).getText().equals(winner)) {
			 mes.setText("X is the winner"); 
		 }
		else if(buttons.get(2).getText().equals(winner) && buttons.get(4).getText().equals(winner) && buttons.get(6).getText().equals(winner)) {
			 mes.setText("X is the winner"); 
		 }
			 
		/////////////////////////////
	 }
	 
	 public static void main(String[] args) {
		 
		 
		 XO xo= new XO("x and o");
		 
		 
		 
	 }

}
