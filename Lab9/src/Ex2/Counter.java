package Ex2;

import java.awt.GridLayout;
import java.awt.event.*;

import javax.swing.*;

public class Counter extends JFrame{

	private int count =0;
	
	public Counter(String title){
		super(title);
		initialize();
		setVisible(true);
	}

	public void initialize(){
		setLayout(new GridLayout(4, 1));
		
		JButton but = new JButton("Count");
		JLabel lab =  new JLabel();
		add(but);
		add(lab);
		setSize(500,500);
		but.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				count++;
				lab.setText(""+count);

			}

		}
		);


	}

	public static void main(String[]args) {
		Counter c = new Counter("counter");
	}
}
