package Ex3;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.*;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
public class FileReaderEx extends JFrame{

	private JTextArea input,output;
	private JButton search;
	
	
	FileReaderEx(String f){
		super(f);
		initialize();
		setVisible(true);
	}
	
	public void initialize(){
		setLayout(new GridLayout(3,1));
		input= new JTextArea("Enter a file access path. Try opening test.txt ");
		input.addMouseListener((MouseListener) new MouseListener() {

			@Override
			public void mouseClicked(MouseEvent e) {
				// TODO Auto-generated method stub
				input.setText("");
			}

			@Override
			public void mousePressed(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void mouseReleased(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void mouseEntered(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void mouseExited(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
		});
		
		output= new JTextArea();
		search= new JButton("Search");
		search.addActionListener((ActionListener)new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				try {
					String filename = input.getText();
					File f= new File(filename);
					FileReader fr =  new FileReader(f);
					BufferedReader br = new BufferedReader(fr);
					int value;
					while((value = br.read()) != -1) {
						String text=output.getText();
						output.setText(text+(char)value);
					}
					
				}
				catch(Exception e1) {e1.printStackTrace();}
				
			}
			
		});
		add(input);
		add(search);
		add(output);
		setSize(500,500);
		
		
	}
	
	public static void main(String[] args) {
		FileReaderEx fr= new FileReaderEx("Filereader");
	}
}
