package Ex3;

public class Counter extends Thread{
	int beg,end;
	
	Counter(int beg, int end){
		this.beg=beg;
		this.end= end;
	}
	
	public void run() {
		
		for(int i=beg;i<end;i++) {
			System.out.println(i);
		}
		
	}

	public static void main(String[] args) {
		
		Counter c1= new Counter(0,100);
		Counter c2 = new Counter(100,200);
		
		System.out.println("First counter:");
		c1.start();
		
		try {
			c1.join();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		System.out.println("Second counter:");
		c2.start();
		try {
			c2.join();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
	}
}
