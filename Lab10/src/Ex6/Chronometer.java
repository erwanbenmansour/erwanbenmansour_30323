package Ex6;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.function.Consumer;

import javax.swing.JButton;
import javax.swing.*;
import javax.swing.JTextArea;

public class Chronometer extends JFrame{
	
	public JLabel time;
	public JButton play ;
	
	public PlayThread playt= null;
	public PauseThread pauset= null;
	
	private static final String ActionEvent = null;
	boolean paused = false;
	
	public int milcount=0;
	public int seccount=0;
	public int mincount=0;
	public int hourcount=0;
	
	
	public Chronometer (String title){
		 
        super(title);
        init();
        setVisible(true);
	}
	 


	void init() {
		
		
		 
		 setLayout(new GridLayout(3,1));
		 time = new JLabel("0:00:00:00");
		 play = new JButton("Pause");
		 play.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				if(paused) {
					play.setText("Pause");
					paused=false;
					System.out.println("Unpaused!");
					
					
					
				}
				else if(!paused){
					play.setText("Play");
					paused=true;
					System.out.println("Paused!");
					
					
				}
			}
		
			 
			 
				 
		 });
		 JButton reset = new JButton("Reset");
		 reset.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(java.awt.event.ActionEvent e) {
				// TODO Auto-generated method stub
				time.setText("0:00:00:00");
				milcount=0;
				seccount=0;
				mincount=0;
				hourcount=0;
				
			}
			 
		 });
		 
		 add(time);
		 add(play);
		 add(reset);
		 
		 setSize(400,400);



		
			
		 
		 
	 }
	
	
	
	public static void main (String[] args) {
		Chronometer c = new Chronometer("chrono");
		PauseThread pauseT= new PauseThread(c);
		PlayThread playT = new PlayThread(c);
		c.pauset=pauseT;
		c.playt=playT;
		playT.start();
		pauseT.start();
		
		
	}


	
		
		
	
	
	
}

