package Ex6;

public class PauseThread extends Thread {
	
	
	Chronometer c;
	
	public PauseThread(Chronometer c){
		this.c=c;
	}
	
	public void run() {
		while(true) {
			
			c.time.setText(c.hourcount+":"+c.mincount+":"+c.seccount+":"+c.milcount);
			if(!c.paused) {
				synchronized(c.playt){
					c.playt.notify();
				}
				
			}
		}
		
	}
	
	
}
