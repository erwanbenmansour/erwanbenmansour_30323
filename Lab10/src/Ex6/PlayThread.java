package Ex6;

public class PlayThread extends Thread{
	
	Chronometer c;
	PlayThread(Chronometer c){
		this.c=c;
	}
	public  void run() {
		// TODO Auto-generated method stub
		
		while(true) {
			
			try {
				Thread.sleep(1);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			c.milcount++;
			if(c.milcount>=1000) {
				c.seccount++;
				c.milcount=0;
			}
			if(c.seccount>=60) {
				c.mincount++;
				c.seccount=0;
			}
			if(c.mincount>=60) {
				c.hourcount++;
				c.mincount=0;
			}
			
			if(c.paused) {
					System.out.println("waiting...");
					synchronized(c.playt) {
						try {
							c.playt.wait();
							
						} catch (InterruptedException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
					}
					
					
					
				
			}
		}
	
		
	}
	
}
