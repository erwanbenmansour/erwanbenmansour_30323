package Ex1;

public class Rectangle extends Shape {
	
	protected double width;
	protected double length;
	
	public Rectangle() {
		width=1.0;
		length=1.0;
		color="rainbow";
		filled=true;
	}
	public Rectangle(double width, double length) {
		super();
		this.width = width;
		this.length = length;
	}

	public Rectangle(String color, boolean filled, double width, double length) {
		super(color, filled);
		this.width = width;
		this.length = length;
	}
	public double getWidth() {
		return width;
	}
	public void setWidth(double width) {
		this.width = width;
	}
	public double getLength() {
		return length;
	}
	public void setLength(double length) {
		this.length = length;
	}
	@Override
	public double getArea() {
		return length*width;
	}
	@Override
	public double getPerimeter() {
		return 2*(length+width);
	}
	@Override
	public String toString() {
		return "The rectangle has a width of"+width+"and a length of "+length+", an area of "+this.getArea()+"a perimeter of "+this.getPerimeter()+"\n";
	}
	
	
	
}
