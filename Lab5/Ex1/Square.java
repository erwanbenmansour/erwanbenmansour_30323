package Ex1;

public class Square extends Rectangle {

	public Square() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Square(double side) {
		super(side, side);
		// TODO Auto-generated constructor stub
	}

	public Square(String color, boolean filled, double side) {
		super(color, filled, side, side);
		// TODO Auto-generated constructor stub
	}
	public double getSide() {
		return width;
	}
	public void setSide(double side) {
		this.width=side;
		this.length=side;
	}
	@Override
	public void setWidth(double side) {
		width=side;
		if(length!=side) {
			length=side;
		}
	}
	@Override
	public void setLength(double side) {
		length=side;
		if(width!=side) width=side;
	}
	@Override
	public String toString() {
		return "The square of side"+getSide()+"has an area of"+getArea()+"and a perimeter of "+getPerimeter()+"\n";
	}
	
	
}
