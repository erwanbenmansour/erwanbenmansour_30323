package Ex4;

public class Controller {

	private TemperatureSensor ts;
	private LightSensor ls;
	
	private static Controller instance = null;
	
	
	
	
	private Controller() {
		
		ts= new TemperatureSensor();
		ls = new LightSensor();
		
		
	}

	
	public static Controller getInstance() {
		if(instance==null) instance = new Controller();
		return instance;
				
				
	}

	public void control() {
		
		int rep=0;
		
		while(rep<20) {
			System.out.println("The value of the Temp Sensor is :"+ts.readValue());
			System.out.println("The value of the Light Sensor is :"+ls.readValue());
			
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
}
