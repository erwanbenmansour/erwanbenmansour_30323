package Ex3;
import java.util.Scanner;

public abstract class Sensor {

	private String location;
	
	
	public Sensor() {
		System.out.println("Give the location value of the sensor");
		Scanner sc = new Scanner(System.in);	
		this.location = sc.nextLine();
	}

	public abstract int readValue();
	
	public String getLocation() {
		return this.location;
	}
}
