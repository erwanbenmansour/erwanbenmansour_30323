package Ex3;

public class Controller {

	private TemperatureSensor ts;
	private LightSensor ls;
	
	
	public Controller() {
		ts= new TemperatureSensor();
		ls = new LightSensor();
	}


	public void control() {
		
		
		
		for(int rep=0;rep<20;rep++) {
			System.out.println("The value of the Temp Sensor is :"+ts.readValue());
			System.out.println("The value of the Light Sensor is :"+ls.readValue());
			
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
	}
}
