package Ex2;

public class ProxyImage implements Image{
	 
	   private Image im;
	   private String fileName;
	   boolean rotated;
	 
	   public ProxyImage(String fileName, boolean rotated){
		  
		  this.rotated=rotated;
		   
	      this.fileName = fileName;
	   }
	 
	   @Override
	   public void display() {
	      if(!rotated ){
	         im = new RealImage(fileName);
	      }
	      else {
	    	im= new RotatedImage(fileName);  
	      }
	      im.display();
	   }
}