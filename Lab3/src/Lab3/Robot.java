package Lab3;

public class Robot {
	
	private int x;
	
	public Robot() {
		x=1;
	}
	
	public void change(int k) {
		if(k>=1) this.x+=k;
	}
	
	public String toString() {
		StringBuffer sb = new StringBuffer();
		sb.append("The position of the robot is ");
		sb.append(x);
		return sb.toString();
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Robot rob1= new Robot();
		rob1.change(5);
		rob1.change(4);
		System.out.println(rob1); //toString is called automatically when printing the object
	}

}
