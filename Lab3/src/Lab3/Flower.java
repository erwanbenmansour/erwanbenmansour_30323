package Lab3;

public class Flower{
    int petal;
    static int numberOfFlowers = 0;
    
    public Flower(){ 
    	System.out.println("Flower has been created!");    
    	numberOfFlowers++;
    }

    public static void main(String[] args) {
    	Flower[] garden = new Flower[5];
		for(int i =0;i<5;i++){
			Flower f = new Flower();
			garden[i] = f;
		}
		
		System.out.println("We created "+numberOfFlowers+" flowers");
	}
}