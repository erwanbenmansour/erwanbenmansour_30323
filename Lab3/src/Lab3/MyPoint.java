package Lab3;

public class MyPoint {

	
	private int x;
	private int y;
	
	public MyPoint(){
		x=0;
		y=0;
	}
	public MyPoint(int x, int y) {
		this.x=x;
		this.y=y;
	}
	
	
	public int getX() {
		return x;
	}
	public void setX(int x) {
		this.x = x;
	}
	public int getY() {
		return y;
	}
	public void setY(int y) {
		this.y = y;
	}
	
	public void setXY(int x, int y){
		this.x=x;
		this.y=y;
	}
	
	public String toString() {
		StringBuffer sb = new StringBuffer();
		sb.append("("+x+","+y+")");
		return sb.toString();
	}
	
	public double distance(int x, int y) {
		return Math.sqrt((this.x-x)*(this.x-x)+(this.y-y)*(this.y-y));
	}
	public double distance(MyPoint p2) {
		return Math.sqrt((this.x-p2.x)*(this.x-p2.x)+(this.y-p2.y)*(this.y-p2.y));
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		MyPoint p1= new MyPoint(1,1);
		MyPoint p2 = new MyPoint(4,1);
		System.out.println(p1.toString());
		System.out.println(p2.toString());
		System.out.println("The distance between the points is "+ p1.distance(p2));
		System.out.println("The distance from the origin is"+ p1.distance(0,0));
		
		
		
	}

}
