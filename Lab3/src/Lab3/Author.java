package Lab3;

public class Author {
	
	private String name;
	private String email;
	private char gender;
	
	public Author(String name, String email, char gender) {
		this.name= name;
		this.email= email;
		this.gender = gender;
	}

	public String getName() {
		return name;
	}


	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public char getGender() {
		return gender;
	}
	@Override //always override toString
	public String toString() {
		StringBuffer sb = new StringBuffer();
		sb.append(name+" ("+gender+") "+"at "+email);
		return sb.toString();	
		}
	
	public static void main(String[]args) {
		Author erwan = new Author("Erwan","erwan.benmansour@gmail.com",'m');
		System.out.println(erwan.toString());
		
	}
}
