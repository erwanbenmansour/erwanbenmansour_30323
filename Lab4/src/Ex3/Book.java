package Ex3;

import Ex2.Author;

public class Book {
	
	private String name;
	private Author author;
	private double price;
	private int qtyInStock = 0;
	
	public Book(String name, Author author, double price) {
		this.name = name;
		this.author = author;
		this.price = price;
	}
	
	public Book(String name, Author author, double price, int qtyInStock) {
		this.name = name;
		this.author = author;
		this.price = price;
		this.qtyInStock = qtyInStock; 
	}
	
	public String getName() {
		return name;
	}
	
	public Author getAuthor() {
		return author;
	}
	
	public double getPrice() {
		return price;
	}
	
	public void setPrice(double price) {
		this.price = price;
	}
	
	public int getQtyInStock() {
		return qtyInStock;
	}
	
	public void setQtyInStock(int qtyInStock) {
		this.qtyInStock = qtyInStock;
	}
	
	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer();
		sb.append(this.name+" by "+this.author);
		return sb.toString();
	}
	
	public static void main(String[] args) {
		
		Author erwan = new Author("Erwan", "erwan.benmansour@gmail.com", 'm');
		Book b1 = new Book("The best book ever", erwan, 999.0);
		System.out.println(b1);
		System.out.println(b1.getName());
		System.out.println(b1.getAuthor());
		System.out.println(b1.getPrice());
		System.out.println(b1.getQtyInStock());
		
		b1.setQtyInStock(5);
		System.out.println(b1.getQtyInStock());
		
		
		
		Author bad = new Author("Bad Author", "bad.author@gmail.com", 'f');
		Book b2 = new Book("The reason I am a bad author", bad, 0.99, 300);
		System.out.println(b2);
		System.out.println(b2.getName());
		System.out.println(b2.getAuthor());
		System.out.println(b2.getPrice());
		System.out.println(b2.getQtyInStock());
		
		b2.setPrice(0.29);
		System.out.println(b2.getPrice());

	}
}
