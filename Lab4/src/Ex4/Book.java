package Ex4;

import Ex2.Author;
public class Book {


	private String name;
	private Author[] author;
	private double price;
	private int qtyInStock = 0;
	
	public Book(String name, Author[] author, double price) {
		this.name = name;
		this.author = author;
		this.price = price;
	}
	
	public Book(String name, Author[] author, double price, int qtyInStock) {
		this.name = name;
		this.author = author;
		this.price = price;
		this.qtyInStock = qtyInStock; 
	}
	
	public String getName() {
		return name;
	}
	
	public Author[] getAuthors() {
		return author;
	}
	
	public double getPrice() {
		return price;
	}
	
	public void setPrice(int price) {
		this.price = price;
	}
	
	public int getQtyInStock() {
		return qtyInStock;
	}
	
	public void setQtyInStock(int qtyInStock) {
		this.qtyInStock = qtyInStock;
	}
	
	public void printAuthors() {
		for(int i=0; i<author.length; i++) {
			System.out.println("author " + (i+1) + ":" + author[i]);
		}
	}
	
	public String toString() {
		return (this.name+" by "+author.length + " authors");
	}
	
	public static void main(String[] args) {
		Author val = new Author("Valentin", "valentin.bouchentouf@gmail.com", 'm');
		Author erwan = new Author("Erwan", "erwan.benmansour@gmail.com", 'm');
		Author alex = new Author("alexi", "alll.xii@yahoo.com", 'f');
		
		Author[] liste = {val, erwan, alex};
		
		Book b1 = new Book("The Best Book With Multiple Authors", liste, 999.0);
		System.out.println(b1.getName());
		
		b1.printAuthors();
		System.out.println(b1);
		System.out.println(b1.getPrice());
		System.out.println(b1.getQtyInStock());

	}
}
