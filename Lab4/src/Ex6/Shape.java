package Ex6;

public class Shape {
	
	private String color = "red";
	private boolean filled = true;
	
	public Shape() {}
	
	public Shape(String color, boolean filled) {
		this.color = color;
		this.filled = filled;
	}

	public String getColor() {
		return color;
	}
	
	public void setColor(String color) {
		this.color = color;
	}
	
	public boolean isFilled(){
		return filled;
	}
	
	public void setfilled(boolean filled) {
		this.filled = filled;
	}
	
	@Override
	public String toString() {
		if (filled) {
			return "A Shape with the color " +  color + " and filled";
		}else {
			return "A Shape with the color " +  color + " and not filled";
		}
		
	}
}
