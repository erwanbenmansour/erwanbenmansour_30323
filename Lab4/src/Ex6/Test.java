package Ex6;

public class Test {
	
	public static void main(String[] args) {
		//test
		Circle c1 = new Circle(3, "red", false);
		System.out.println(c1);
		
		Rectangle r = new Rectangle(5, 4, "blue", true);
		
		System.out.println(r);
		r.setColor("green");
		r.setLength(2);
		System.out.println(r);
		
		Square s = new Square(6, "pink", false);
		System.out.println(s);
		s.setSide(2);
		System.out.println(s);
		
		
		s.setLength(3);
		s.setWidth(9);
		System.out.println(s);
	}
}
