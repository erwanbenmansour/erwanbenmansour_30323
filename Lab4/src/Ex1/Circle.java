package Ex1;

public class Circle {
	
	private double radius = 1.0;
	
	private String color = "red";
	
	public Circle() {
	}
	
	public Circle(double radius) {
		this.radius = radius;
	}
	
	public double getRadius() {
		return radius;
	}
	
	public double getArea() {
		return Math.PI * radius * radius;
	}
	
	public String toString() {
		return "r:" + radius + ", color:" + color; 
	}
	
	
	public static void main(String[] args) {
		Circle c1 = new Circle();
		System.out.println(c1);
		
		Circle c2 = new Circle(10);
		System.out.println(c2);
		
		System.out.println(c1.getRadius());
		
		System.out.println(c1.getArea());
		System.out.println(c2.getArea());

	}

}
