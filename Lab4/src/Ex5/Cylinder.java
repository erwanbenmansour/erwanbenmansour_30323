package Ex5;

import Ex1.Circle;
public class Cylinder extends Circle {
	
	
	private double height = 1.0;
	
	public Cylinder() {
		super();
	}
	
	public Cylinder(double radius) {
		super(radius);
	}
	
	public Cylinder(double radius, double height) {
		super(radius);
		this.height = height;
	}
	
	public double getHeight() {
		return height;
	}
	
	public double getVolume() {
		return super.getArea() * height;
	}
	
	@Override
	public double getArea() {
		return 2 * Math.PI * Math.pow(this.getRadius(), 2) + height * 2 * Math.PI * this.getRadius(); 
	}
	
	public static void main(String[] args) {
		Cylinder c1 = new Cylinder();
		System.out.println(c1.getHeight());
		System.out.println(c1.getArea());
		
		Cylinder c2 = new Cylinder(3, 2);
		System.out.println(c2.getRadius());
		System.out.println(c2.getHeight());
		System.out.println(c2.getArea());
		System.out.println(c2.getVolume());
		
	}
}
