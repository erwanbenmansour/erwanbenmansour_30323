package Ex2;

import java.awt.EventQueue;


import javax.swing.JFrame;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.util.*;
import java.awt.event.ActionEvent;
import java.awt.Font;

public class View implements Observer{

	public JFrame frame;
	
	public JButton addButton,viewProductsButton,deleteButton,changeQuantityButton;
	
	public Controller c;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		
		StockSystem s = new StockSystem();
		View v = new View();
		v.frame.setVisible(true);
		
		
		Controller c = new Controller(v,s);
		
		
		
	}

	/**
	 * Create the application.
	 */
	public View() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	public void initialize() {
		if(frame==null) frame = new JFrame();
		else {
			frame.getContentPane().invalidate();
			frame.getContentPane().removeAll();
			frame.getContentPane().revalidate();
			frame.getContentPane().repaint();
		}
		frame.setBounds(100, 100, 625, 449);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		addButton = new JButton("Add Product");
		addButton.setBounds(89, 102, 176, 23);
		frame.getContentPane().add(addButton);
		
		viewProductsButton = new JButton("View Products");
		viewProductsButton.setBounds(89, 217, 176, 23);
		frame.getContentPane().add(viewProductsButton);
		
		deleteButton = new JButton("Delete Product");
		deleteButton.setBounds(343, 102, 176, 23);
		frame.getContentPane().add(deleteButton);
		
		changeQuantityButton = new JButton("Change Product Quantity");
		changeQuantityButton.setBounds(343, 217, 176, 23);
		frame.getContentPane().add(changeQuantityButton);
		
		
		
	}
	
	

	@Override
	public void update(Observable o, Object arg) {
		// TODO Auto-generated method stub
		
	}
}
