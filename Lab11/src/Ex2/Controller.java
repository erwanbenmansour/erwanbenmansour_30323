package Ex2;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class Controller {

	View view;
	StockSystem model;
	
	public Controller(View v, StockSystem s) {
		view=v;
		model= s;
		model.addObserver(view);
		view.c=this;
		addListeners();
	}
	
	public void addListeners() {
		view.addButton.addActionListener(new AddButtonListener());
		view.deleteButton.addActionListener(new DeleteButtonListener());
		view.viewProductsButton.addActionListener(new DisplayButtonListener());
		view.changeQuantityButton.addActionListener(new QuantityButtonListener());
	}
	
	//addButton Effect
	class AddButtonListener implements ActionListener{

		public void actionPerformed(ActionEvent e) {
			
			JFrame frame= view.frame;
			frame.getContentPane().invalidate();
			frame.getContentPane().removeAll();
			frame.getContentPane().revalidate();
			frame.getContentPane().repaint();
			
			frame.setBounds(100, 100, 582, 422);
			frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			frame.getContentPane().setLayout(null);
			
			JTextField txtProductName;
			JTextField txtProductQuantity;
			JTextField txtPrice;
			txtProductName = new JTextField();
			txtProductName.setText("Name");
			txtProductName.setBounds(224, 100, 96, 20);
			frame.getContentPane().add(txtProductName);
			txtProductName.setColumns(10);
			
			txtProductQuantity = new JTextField();
			txtProductQuantity.setText("Quantity");
			txtProductQuantity.setBounds(224, 165, 96, 20);
			frame.getContentPane().add(txtProductQuantity);
			txtProductQuantity.setColumns(10);
			
			txtPrice = new JTextField();
			txtPrice.setText("Price");
			txtPrice.setColumns(10);
			txtPrice.setBounds(224, 231, 96, 20);
			frame.getContentPane().add(txtPrice);
			
			JButton btnNewButton = new JButton("Add Product");
			btnNewButton.setBounds(200, 295, 140, 23);
			frame.getContentPane().add(btnNewButton);
			btnNewButton.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					// TODO Auto-generated method stub
					Product p= new Product(txtProductName.getText(),Integer.parseInt(txtProductQuantity.getText()),Double.parseDouble(txtPrice.getText()));
					model.addProduct(p);
					view.initialize();
					addListeners();
				}
				
			});

		}

		}
	
	//DeleteButtonListener
	class DeleteButtonListener implements ActionListener{

		public void actionPerformed(ActionEvent e) {
			
			JFrame frame= view.frame;
			frame.getContentPane().invalidate();
			frame.getContentPane().removeAll();
			frame.getContentPane().revalidate();
			frame.getContentPane().repaint();
			
			frame.setBounds(100, 100, 582, 422);
			frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			frame.getContentPane().setLayout(null);
			
			JTextField txtProductName;
			JTextField txtProductQuantity;
			JTextField txtPrice;
			txtProductName = new JTextField();
			txtProductName.setText("Name");
			txtProductName.setBounds(224, 100, 96, 20);
			frame.getContentPane().add(txtProductName);
			txtProductName.setColumns(10);
			
			txtProductQuantity = new JTextField();
			txtProductQuantity.setText("Quantity");
			txtProductQuantity.setBounds(224, 165, 96, 20);
			frame.getContentPane().add(txtProductQuantity);
			txtProductQuantity.setColumns(10);
			
			txtPrice = new JTextField();
			txtPrice.setText("Price");
			txtPrice.setColumns(10);
			txtPrice.setBounds(224, 231, 96, 20);
			frame.getContentPane().add(txtPrice);
			
			JButton btnNewButton = new JButton("Delete Product");
			btnNewButton.setBounds(200, 295, 140, 23);
			frame.getContentPane().add(btnNewButton);
			btnNewButton.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					// TODO Auto-generated method stub
				
					String name= txtProductName.getText();
						
						int quantity = Integer.parseInt(txtProductQuantity.getText());
						double price= Double.parseDouble(txtPrice.getText());
						for(Product p : model.products) {
							if(p.name==name && p.price==price && p.quantity==quantity) model.deleteProduct(p);
					}
				
					
					view.initialize();
					addListeners();
				}
				
			});

		}

		}
	
	
	
	class DisplayButtonListener implements ActionListener{

		public void actionPerformed(ActionEvent e) {
			
			JFrame frame= view.frame;
			frame.getContentPane().invalidate();
			frame.getContentPane().removeAll();
			frame.getContentPane().revalidate();
			frame.getContentPane().repaint();
			
			
			frame.setBounds(100, 100, 582, 422);
			frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			frame.getContentPane().setLayout(null);
			
			int i=0;
			for(Product p : model.products) {
				
				JTextArea txtNewText = new JTextArea("Name: "+p.name+"\nQuantity: "+p.quantity+"\nPrice: "+p.price);
				txtNewText.setBounds(225, 40+i, 100, 70);
				txtNewText.setEditable(false);
				frame.getContentPane().add(txtNewText);
				i+=200;
			}
			
			
			JButton btnNewButton = new JButton("Home Page");
			btnNewButton.setBounds(10, 11, 548, 23);
			frame.getContentPane().add(btnNewButton);
			btnNewButton.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					// TODO Auto-generated method stub
					view.initialize();
					addListeners();
				}
				
			});

		}

		}
	
	
	

	class QuantityButtonListener implements ActionListener{

		public void actionPerformed(ActionEvent e) {
			
			JFrame frame= view.frame;
			frame.getContentPane().invalidate();
			frame.getContentPane().removeAll();
			frame.getContentPane().revalidate();
			frame.getContentPane().repaint();
			
			
			frame.setBounds(100, 100, 582, 422);
			frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			frame.getContentPane().setLayout(null);
			
			int i=0;
			for(Product prod : model.products) {
				
				String name= prod.name;
			
				JButton txtNewText = new JButton("Name: "+name);
				txtNewText.setBounds(200, 40+i, 200, 100);
				frame.getContentPane().add(txtNewText);
				i+=200;
				txtNewText.addActionListener(new ActionListener() {

					@Override
					public void actionPerformed(ActionEvent e) {
						// TODO Auto-generated method stub
						JFrame frame= view.frame;
						frame.getContentPane().invalidate();
						frame.getContentPane().removeAll();
						frame.getContentPane().revalidate();
						frame.getContentPane().repaint();
						
						
						frame.setBounds(100, 100, 582, 422);
						frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
						frame.getContentPane().setLayout(null);
						
						
						JButton btnNewButton = new JButton("Home Page");
						btnNewButton.setBounds(10, 11, 548, 23);
						frame.getContentPane().add(btnNewButton);
						btnNewButton.addActionListener(new ActionListener() {

							@Override
							public void actionPerformed(ActionEvent e) {
								// TODO Auto-generated method stub
								view.initialize();
								addListeners();
							}
							
						});
						
						
						JTextField textField_1 = new JTextField();
						textField_1.setBounds(251, 187, 48, 20);
						frame.getContentPane().add(textField_1);
						textField_1.setColumns(10);
						
						JButton btnNewButton_1 = new JButton("Change Quantity");
						btnNewButton_1.setBounds(199, 254, 146, 23);
						frame.getContentPane().add(btnNewButton_1);
						btnNewButton_1.addActionListener(new ActionListener() {

							@Override
							public void actionPerformed(ActionEvent e) {
								// TODO Auto-generated method stub
								for(Product p : model.products) {
									if(p.name==prod.name) {
										prod.quantity=Integer.parseInt(textField_1.getText());
									};
								}
							}
							
						});
					}
					
				});
			}
			
			
			JButton btnNewButton = new JButton("Home Page");
			btnNewButton.setBounds(10, 11, 548, 23);
			frame.getContentPane().add(btnNewButton);
			btnNewButton.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					// TODO Auto-generated method stub
					view.initialize();
					addListeners();
				}
				
			});

		}

		}
}
