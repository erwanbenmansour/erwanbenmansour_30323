package Ex2;

import java.util.ArrayList;
import java.util.Observable;

public class StockSystem extends Observable{

	ArrayList<Product> products = new ArrayList<Product>();
	
	public void addProduct(Product p) {
		products.add(p);
	}
	public void viewProducts() {
		for(Product p: products) {
			System.out.println(p);
		}
	}
	
	public void deleteProduct(Product p) {
		products.remove(p);
	}
	
	public void changeQuatity(Product p, int q) {
		p.quantity=q;
	}
}
