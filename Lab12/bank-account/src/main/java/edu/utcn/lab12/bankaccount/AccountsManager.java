package edu.utcn.lab12.bankaccount;

import java.util.ArrayList;

public class AccountsManager {
	
	ArrayList<BankAccount> bank = new ArrayList<BankAccount>();

    public void addAccount(BankAccount account){
    	bank.add(account);
    }

    public boolean exists(String id){
    	for(BankAccount b : bank) {
    		if(b.getId()==id) {
    			return true;
    		}
    	}
    	return false;
    
    }

    public int count(){
        return bank.size();
    }
}
