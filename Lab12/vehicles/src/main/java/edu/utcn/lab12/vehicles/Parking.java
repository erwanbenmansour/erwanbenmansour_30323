package edu.utcn.lab12.vehicles;

import java.util.ArrayList;


public class Parking {

    /**
     * Vehicles will be parked in parkedVehicles array.
     */
    ArrayList<Vehicle> parkedVehicles = new ArrayList<Vehicle>();

    public void parkVehicle(Vehicle e){
    	parkedVehicles.add(e);
    }

    /**
     * Sort vehicles by length.
     */
    public void sortByWeight(){
    	boolean flag=true;
    	while(flag) {
    		for(int i=0;i<parkedVehicles.size()-1;i++) {
    			if(parkedVehicles.get(i).weight>parkedVehicles.get(i+1).weight) {
    				Vehicle temp=parkedVehicles.get(i);
    				parkedVehicles.set(i,parkedVehicles.get(i+1));
    				parkedVehicles.set(i+1,temp);
    				
    			}
    		}
    	}
    }

    public Vehicle get(int index){
        return parkedVehicles.get(index);
    }

}
