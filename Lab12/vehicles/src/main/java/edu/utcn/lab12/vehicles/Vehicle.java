package edu.utcn.lab12.vehicles;

import java.util.Objects;

public class Vehicle {
    private String type;
    public int weight;

    public Vehicle(String type, int weight) {
        this.type = type;
        this.weight = weight;
    }

	@Override
	public int hashCode() {
		return Objects.hash(type, weight);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		
		Vehicle other = (Vehicle) obj;
		return Objects.equals(type, other.type) && weight == other.weight;
	}

}
