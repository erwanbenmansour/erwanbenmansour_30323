package edu.utcn.lab12.vehicles;

public class ElectricBattery {

    /**
     * Percentage load.
     */
    private int charge = 0;

    public void charge() throws BatteryException{
    	if(charge==100) {
    		throw new BatteryException("AlreadyCharged");
    	}
        charge++;
    }

}
