package edu.utcn.lab12.vehicles;

import java.util.Objects;

public class Vehicle {

    private String type;
    public int weight;

    public Vehicle(String type, int length) {
        this.type = type;
    }

    public String start(){
        return "engine started";
    }

	@Override
	public int hashCode() {
		return Objects.hash(type, weight);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Vehicle other = (Vehicle) obj;
		return type.equals(other.type) && weight == other.weight;
	}
  
  

}
