package edu.utcn.lab12.vehicles;

import java.util.ArrayList;

public class Parking {

	ArrayList<Vehicle> parking = new ArrayList<Vehicle>();
	
    public void parkVehicle(Vehicle e){
    	parking.add(e);
    }

    /**
     * Sort vehicles by length.
     */
    public void sortByWeight(){
    	boolean flag=true;
    	while(flag) {
    		for(int i=0;i<parking.size()-1;i++) {
    			if(parking.get(i).weight>parking.get(i+1).weight) {
    				Vehicle temp=parking.get(i);
    				parking.set(i,parking.get(i+1));
    				parking.set(i+1,temp);
    				
    			}
    		}
    	}
    	
    }

    public Vehicle get(int index){
        return parking.get(index);
    }

}
