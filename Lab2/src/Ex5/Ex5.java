package Ex5;
import java.util.*;
public class Ex5 {

	public static void main(String[] args) {
		
		List<Integer> vect = new ArrayList<Integer>();
		Random rand = new Random();
		for(int i =0;i<10;i++) {
			int num=rand.nextInt(100);
			vect.add(num);
		}
		
		boolean change=true;
		while(change) {
			change = false;
			for(int i = 0; i<vect.size()-1;i++) {
				if(vect.get(i)>vect.get(i+1)) {
					int temp= vect.get(i+1);
					vect.set(i+1,vect.get(i));
					vect.set(i, temp);
					change= true;
				}
				
			}
		}
		System.out.println("The ordered random list is "+vect);
	}

}
