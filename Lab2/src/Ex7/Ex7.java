package Ex7;

import java.util.*;

public class Ex7 {
	
	public static void main(String[]args ) {
		
		Random rand = new Random();
		int numToGuess = rand.nextInt(10);
		boolean guessed =false;
		int tries= 0;
		while(guessed==false) {
			if(tries<3) {
				System.out.println("Guess the number?");
				Scanner sc= new Scanner(System.in);
				int guess=sc.nextInt();
				if(guess>numToGuess) System.out.println("Wrong answer, your number it too high");
				else if(guess<numToGuess) System.out.println("Wrong answer, your number is too low");
				else {
					guessed=true;
					System.out.println("You win!");
				}
				tries++;
				
				
			}
			else {
				System.out.println("You lost");
				guessed=true;
			}
		}
	}
	
}
