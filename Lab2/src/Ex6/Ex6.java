package Ex6;




public class Ex6 {

	
	
	
	public static int calculateFactorial(int n) {
		
		if(n==1) {
			return 1;
		}
		else return n*calculateFactorial(n-1);
		
	}
	
	public static void main(String[] args) {
		int n =5;
		
		int res=n;
		for(int i=n-1; i>0;i--) {
			res=res*i;
		}
		System.out.println("The factorial of 5 without recursive is is "+ res);
		
		System.out.println("The factorial of 5 with recursive is is "+calculateFactorial(n));
		
		
		
		
	}
}
