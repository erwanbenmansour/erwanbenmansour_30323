package Ex3;
import java.util.*;

public class Ex3 {

	public static void main(String[] args) {
		/*
		int[] arr = new int[10];
		Random r = new Random();
		
		for(int i = 0; i<10;i++) {
			arr[i] = r.nextInt(100);
		}
		
		int max =0;
		Arrays.sort(arr);
		for(int i = 0; i<10;i++) {
			System.out.println(arr[i]+"\n");
		}
		*/
		
		
		System.out.println("Give two numbers");
		
		Scanner sc= new Scanner(System.in);
		
		int nb1= sc.nextInt();
		int nb2= sc.nextInt();
		
		
		List<Integer> primes = new ArrayList<Integer>();
		System.out.println(nb1);
		if(nb1>=nb2) {
			for(int i=nb2;i<nb1;i++) {
				boolean prime=true;
				for(int j=2;j<=Math.sqrt(i);j++ ) {
					if(i%j==0) {
						prime=false;
					}
				}
				
				if(prime==true) {
					primes.add(i);
				}
			}
		}
		else if(nb2>=nb1) {
			for(int i=nb1;i<nb2;i++) {
				boolean prime=true;
				for(int j=2;j<=Math.sqrt(i);j++ ) {
					if(i%j==0) {
						prime=false;
					}
				}
				if(prime==true) {
					primes.add(i);
				}
			}
		}
		
		System.out.println(primes);
		
	}

}
