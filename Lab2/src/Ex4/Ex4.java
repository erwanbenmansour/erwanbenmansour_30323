package Ex4;

import java.util.*;

public class Ex4 {
	
	public static void main(String[] args) {
		
		int[] arr = new int[]{2,19,87,26,92,74,26,37,67,11};
		int max=arr[0];
		
		for(int element : arr) {
			if(element>max) {
				max=element;
			}
		}
		System.out.println("The maximulm is "+ max);
	}
}
