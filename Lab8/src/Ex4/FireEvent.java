package Ex4;


class FireEvent extends Event {

    private boolean smoke;
    private String room;

    FireEvent(boolean smoke,String room) {
        super(EventType.FIRE);
        this.smoke = smoke;
        this.room=room;
    }

    boolean isSmoke() {
        return smoke;
    }

    public String getRoom() {
        return room;
    }

    @Override
    public String toString() {
        return "FireEvent{" + "smoke=" + smoke + '}';
    }

}