package Ex4;

public class FireSensor {
    private String room;
    public FireSensor(String room){
        this.room=room;
    }

    public boolean isFireDetected(FireEvent event){
        if(event.getRoom().equals(room)){
            if (event.isSmoke()){
                return true;
            }
        }
        return false;
    }

    public String getRoom() {
        return room;
    }
}

