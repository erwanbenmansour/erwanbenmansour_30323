package Ex4;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

import static Ex4.EventType.*;



public class ControlUnit 
    private FileWriter fw;
    private AlarmUnit alarm=new AlarmUnit();
    private CoolingUnit CoolingUnit=new CoolingUnit();
    private FireSensor FireSensor1=new FireSensor("Kitchen");
    private FireSensor FireSensor2=new FireSensor("Bedroom");
    private FireSensor FireSensor3=new FireSensor("Bathroom");
    private ArrayList<FireSensor> FireSensorList=new ArrayList<FireSensor>();
    private GsmUnit Gsm=new GsmUnit();
    private HeatingUnit HeatingUnit=new HeatingUnit();
    private TemperatureSensor temperatureSensor=new TemperatureSensor();
    private int temp;
    private static ControlUnit instance;


    private ControlUnit(String path,int temp){
        try {
            fw=new FileWriter(path);
        } catch (IOException e) {
            e.printStackTrace();
        }
        this.temp=temp;
        FireSensorList.add(FireSensor1);
        FireSensorList.add(FireSensor2);
        FireSensorList.add(FireSensor3);
    };

    public static ControlUnit getInstance(String path,int temp)
    {
        if (instance==null)
            instance = new ControlUnit(path,temp);
        return instance;
    }

    public void checkSensors(Event event){
        if (event.getType()==TEMPERATURE){
            if(temperatureSensor.CheckTemperature((TemperatureEvent)event,temp)==1){
                CoolingUnit.setCoolingOn();
                try {
                    fw.write("Cooling system ON \n");
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            else if(temperatureSensor.CheckTemperature((TemperatureEvent)event,temp)==-1){
                HeatingUnit.setHeatingOn();
                try {
                    fw.write("Heating system ON \n");
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        else if(event.getType()==FIRE){
            for (FireSensor sensor: FireSensorList
                 ) {
                if (sensor.isFireDetected((FireEvent) event)){
                    alarm.ringTheAlarm();
                    try {
                        fw.write("Fire detected in "+sensor.getRoom()+" alarm ON \n");
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    public void close(){
        try {
            fw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
