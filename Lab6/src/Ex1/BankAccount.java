package Ex1;

import java.util.Objects;

public class BankAccount {

	private String owner;
	private double balance;
	
	
	public BankAccount(String owner, double balance) {
		this.owner = owner;
		this.balance = balance;
	}
	
	public void withdraw(double amount) {
		this.balance-=amount;
	}
	public void deposit(double amount) {
		this.balance+=amount;
	}
	
	
	
	@Override
	public int hashCode() {
		return Objects.hash(balance, owner);
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BankAccount other = (BankAccount) obj;
		return Double.doubleToLongBits(balance) == Double.doubleToLongBits(other.balance)
				&& Objects.equals(owner, other.owner);
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		BankAccount b1= new BankAccount("Erwan",999.00);
		BankAccount b2=new BankAccount("Remi",999.00);
		BankAccount b3=new BankAccount("Erwan",1000.00);
		BankAccount b4= new BankAccount("Erwan",999.00);
		System.out.println(b1.equals(b2));
		System.out.println(b1.equals(b3));
		System.out.println(b1.equals(b4));
		
	}

}
