package Ex3;
import java.util.*;

public class Bank {

	Comparator<BankAccount> cmp = new Comparator<BankAccount>(){

		@Override
		public int compare(BankAccount o1, BankAccount o2) {
			// TODO Auto-generated method stub
			
			if(o1.balance>o2.balance) return 1;
			if(o1.balance<o2.balance) return -1;
			
			
			return 0;
		}
		
	};
	private Set<BankAccount> accounts = new TreeSet<BankAccount>(cmp);
	
	
	public void printAccounts() {
		Iterator<BankAccount> it = accounts.iterator();
		while(it.hasNext()) {
			System.out.println(it.next());
		}
		
	}
	
	public void printAccounts(double min, double max) {
		Iterator<BankAccount> it = accounts.iterator();
		while(it.hasNext()) {
			BankAccount val = it.next();
			if(val.balance<max && val.balance>min) {
				System.out.println(val);
			}
			
		}
		
	}
	
	public void addAccount(BankAccount b) {
		this.accounts.add(b);
	}
	
	
	public TreeSet<BankAccount> getAllAccounts(){
		
		Comparator<BankAccount> cmp = new Comparator<BankAccount>(){

			@Override
			public int compare(BankAccount o1, BankAccount o2) {
				// TODO Auto-generated method stub
				
				return o1.owner.compareTo(o2.owner);
			}
			
		};
		TreeSet<BankAccount> newAccounts = new TreeSet<BankAccount>(cmp);
		
		Iterator<BankAccount> it =accounts.iterator();
		while(it.hasNext()) {
			BankAccount val = it.next();
			
			System.out.println(val);
			
			newAccounts.add(val);
			
			
		}
		
		return newAccounts;
		
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Bank bank = new Bank();
		BankAccount b1= new BankAccount("Erwan",999.00);
		BankAccount b2=new BankAccount("Remi",209.00);
		BankAccount b3=new BankAccount("Erwan",1999.00);
		BankAccount b4= new BankAccount("Erwan",999.00);
		
		System.out.println(b1.equals(b2));
		System.out.println(b1.equals(b3));
		System.out.println(b1.equals(b4));
		
		bank.accounts.add(b1);
		bank.accounts.add(b2);
		bank.accounts.add(b3);
		bank.accounts.add(b4);
		
		//Get accounts sorted by balance
		bank.printAccounts();
		
		//Get accounts with balance between numbers
		bank.printAccounts(500,1000);
		
		//Get accounts in alphabetical order
		bank.getAllAccounts();
		
		
		
		
	}
}
