package Ex4;

import java.util.Scanner;

public class ConsoleMenu {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Dictionary dico = new Dictionary();
		String answer="y";
		
		
		while(answer.equals("y")) {
			
			
			
			System.out.println("Do you want to add a word? Type 'y' for yes and 'n' for no\n");
			Scanner sc = new Scanner(System.in);
			answer=sc.next();
			if(answer.equals("y")) {
				Word w = new Word();
				Definition d = new Definition();
				System.out.println("What's the word?");
				Scanner sc1 = new Scanner(System.in);
				w.name=sc1.next();
				System.out.println("What's the definition?");
				Scanner sc2 = new Scanner(System.in);
				d.description=sc2.next();
				dico.addWord(w, d);
				
			}
			
			//Doesn't work for some reason
			System.out.println("Do you want to get a definition? Type 'y' for yes and 'n' for no\n");
			Scanner sc3 = new Scanner(System.in);
			answer=sc3.next();
			if(answer.equals("y")) {
				Word w = new Word();
				System.out.println("What's the word?");
				Scanner sc4 = new Scanner(System.in);
				w.name=sc4.next();
				System.out.println(dico.getDefinition(w).description);
				
				
			}
			
			System.out.println("Do you want to get all words? Type 'y' for yes and 'n' for no\n");
			Scanner sc5 = new Scanner(System.in);
			answer=sc5.next();
			if(answer.equals("y")) {
				dico.getAllWords();
				
				
			}
			
			

			System.out.println("Do you want to get all definitions? Type 'y' for yes and 'n' for no\n");
			Scanner sc6 = new Scanner(System.in);
			answer=sc6.next();
			if(answer.equals("y")) {
				dico.getAllDefinitions();
				
				
			}
			
			
			System.out.println("Do you want to continue interacting? Type 'y' for yes and 'n' for no\n");
			Scanner sc7 = new Scanner(System.in);
			answer=sc7.next();
			
			
			
			
			
			
			
		}
	}

}
