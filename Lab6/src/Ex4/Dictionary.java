package Ex4;
import java.util.*;
public class Dictionary {
	
	HashMap<Word,Definition> entries = new HashMap<Word,Definition>();

	public void addWord(Word w, Definition d) {
		entries.put(w, d);
	}
	public Definition getDefinition(Word w) {
		System.out.println(entries.get(w).description);
		return entries.get(w);
	}
	public void getAllWords() {
		for(Word w : entries.keySet()) {
			System.out.println(w.name);
		}
	}
	public void getAllDefinitions() {
		for(Definition d : entries.values()) {
			System.out.println(d.description);
		}
	}
	
}
