package Ex2;
import java.util.*;

public class Bank {

	private List<BankAccount> accounts = new ArrayList<BankAccount>();
	
	
	public void printAccounts() {
		Comparator<BankAccount> cmp = new Comparator<BankAccount>(){

			@Override
			public int compare(BankAccount o1, BankAccount o2) {
				// TODO Auto-generated method stub
				
				if(o1.balance>o2.balance) return 1;
				if(o1.balance<o2.balance) return -1;
				
				
				return 0;
			}
			
		};
		accounts.sort(cmp);
		for(int i=0;i<accounts.size();i++) {
			System.out.println(accounts.get(i));
		}
		
	}
	
	public void printAccounts(double min, double max) {
		Comparator<BankAccount> cmp = new Comparator<BankAccount>(){

			@Override
			public int compare(BankAccount o1, BankAccount o2) {
				// TODO Auto-generated method stub
				
				if(o1.balance>o2.balance) return 1;
				if(o1.balance<o2.balance) return -1;
				
				
				return 0;
			}
			
		};
		accounts.sort(cmp);
		for(int i=0;i<accounts.size();i++) {
			if(accounts.get(i).balance>min && accounts.get(i).balance<max) System.out.println(accounts.get(i));
			
		}
		
	}
	
	public void addAccount(BankAccount b) {
		this.accounts.add(b);
	}
	public BankAccount getAccount(String owner) {
		
		
		for(int i=0;i<accounts.size();i++) {
			if(accounts.get(i).owner==owner) {
				return accounts.get(i);
			}
			
		}
		return null;
		
	}
	
	public List<BankAccount> getAllAccounts(){
		
		Comparator<BankAccount> cmp = new Comparator<BankAccount>(){

			@Override
			public int compare(BankAccount o1, BankAccount o2) {
				// TODO Auto-generated method stub
				
				return o1.owner.compareTo(o2.owner);
			}
			
		};
		accounts.sort(cmp);
		
		for(int i=0;i<accounts.size();i++) {
			System.out.println(accounts.get(i));
		}
		
		return accounts;
		
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Bank bank = new Bank();
		BankAccount b1= new BankAccount("Erwan",999.00);
		BankAccount b2=new BankAccount("Remi",209.00);
		BankAccount b3=new BankAccount("Erwan",1999.00);
		BankAccount b4= new BankAccount("Erwan",999.00);
		
		System.out.println(b1.equals(b2));
		System.out.println(b1.equals(b3));
		System.out.println(b1.equals(b4));
		
		bank.accounts.add(b1);
		bank.accounts.add(b2);
		bank.accounts.add(b3);
		bank.accounts.add(b4);
		
		//Get accounts sorted by balance
		bank.printAccounts();
		
		//Get accounts with balance between numbers
		bank.printAccounts(500,1000);
		
		//Get accounts in alphabetical order
		bank.getAllAccounts();
		
		
		
		
	}
}
