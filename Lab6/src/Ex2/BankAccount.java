package Ex2;

import java.util.Objects;


public class BankAccount {

	public String owner;
	public double balance;
	
	
	public BankAccount(String owner, double balance) {
		this.owner = owner;
		this.balance = balance;
	}
	
	public void withdraw(double amount) {
		this.balance-=amount;
	}
	public void deposit(double amount) {
		this.balance+=amount;
	}
	
	
	
	@Override
	public int hashCode() {
		return Objects.hash(balance, owner);
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BankAccount other = (BankAccount) obj;
		return Double.doubleToLongBits(balance) == Double.doubleToLongBits(other.balance)
				&& Objects.equals(owner, other.owner);
	}

	@Override
	public String toString() {
		return "BankAccount [owner=" + owner + ", balance=" + balance + "]";
	}
	

}
